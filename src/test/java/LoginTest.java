
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class LoginTest {

    private WebDriver driver;

    @Before
    public void setUp() throws Exception {
        driver = new FirefoxDriver();
        driver.get("http://rolniko.lizardsgames.com/index.php");
        driver.findElement(By.linkText("Logowanie")).click();
    }

    @Test
    public void testValidUsernameValidPassword() throws Exception {
        driver.findElement(By.name("username")).sendKeys("marcelina");
        driver.findElement(By.name("password")).sendKeys("Qaz12345");
        driver.findElement(By.cssSelector("input[value=\"Zaloguj się\"]")).click();

        assertEquals("Wyloguj się", driver.findElement(By.linkText("Wyloguj się")).getText());
    }

    @Test
    public void testValidUsernameInvalidPassword() throws Exception {
        driver.findElement(By.name("username")).sendKeys("marcelina");
        driver.findElement(By.name("password")).sendKeys("Qaz1");
        driver.findElement(By.cssSelector("input[value=\"Zaloguj się\"]")).click();

        assertTrue(driver.getPageSource().contains("Zły login lub hasło."));
    }

    @Test
    public void testInvalidUsernameValidPassword() throws Exception {
        driver.findElement(By.name("username")).sendKeys("marceli");
        driver.findElement(By.name("password")).sendKeys("Qaz12345");
        driver.findElement(By.cssSelector("input[value=\"Zaloguj się\"]")).click();

        assertTrue(driver.getPageSource().contains("Zły login lub hasło."));
    }
    
    @Test
    public void testInvalidUsernameInvalidPassword() throws Exception {
        driver.findElement(By.name("username")).sendKeys("marceli");
        driver.findElement(By.name("password")).sendKeys("Qaz1");
        driver.findElement(By.cssSelector("input[value=\"Zaloguj się\"]")).click();

        assertTrue(driver.getPageSource().contains("Zły login lub hasło."));
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
    }
}
