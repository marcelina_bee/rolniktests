
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class RegisterTest {

    private WebDriver driver;

    @Before
    public void setUp() throws Exception {
        driver = new FirefoxDriver();
        driver.get("http://rolniko.lizardsgames.com/index.php");
        driver.findElement(By.linkText("Rejestracja")).click();
    }

    @Test
    public void testUsingCorrectData() throws Exception {
        driver.findElement(By.name("username")).sendKeys("m" + (System.currentTimeMillis() / 1000L));
        driver.findElement(By.name("password")).sendKeys("Qaz12345");
        driver.findElement(By.name("retype_password")).sendKeys("Qaz12345");
        driver.findElement(By.cssSelector("input[value=\"Zarejestruj się\"]")).click();

        assertEquals("Wyloguj się", driver.findElement(By.linkText("Wyloguj się")).getText());
    }

    @Test
    public void testUsingEmptyUsername() throws Exception {
        driver.findElement(By.name("username")).sendKeys("");
        driver.findElement(By.name("password")).sendKeys("Qaz12345");
        driver.findElement(By.name("retype_password")).sendKeys("Qaz12345");
        driver.findElement(By.cssSelector("input[value=\"Zarejestruj się\"]")).click();

        assertTrue(driver.getPageSource().contains("Wpisz nazwę użtkownika."));
    }

    @Test
    public void testUsingEmptyPassword() throws Exception {
        driver.findElement(By.name("username")).sendKeys("m" + (System.currentTimeMillis() / 1000L));
        driver.findElement(By.name("password")).sendKeys("");
        driver.findElement(By.name("retype_password")).sendKeys("");
        driver.findElement(By.cssSelector("input[value=\"Zarejestruj się\"]")).click();

        assertTrue(driver.getPageSource().contains("Hasło musi mieć co najmniej 6 znaków"));
    }

    @Test
    public void testUsingNotUniqueUsername() throws Exception {
        driver.findElement(By.name("username")).sendKeys("marcelina");
        driver.findElement(By.name("password")).sendKeys("Qaz12345");
        driver.findElement(By.name("retype_password")).sendKeys("Qaz12345");
        driver.findElement(By.cssSelector("input[value=\"Zarejestruj się\"]")).click();

        assertTrue(driver.getPageSource().contains("Nazwa użtkownika jest już zajęta"));
    }

    @Test
    public void testUsingDifferentPasswords() throws Exception {
        driver.findElement(By.name("username")).sendKeys("m" + (System.currentTimeMillis() / 1000L));
        driver.findElement(By.name("password")).sendKeys("Qaz12345");
        driver.findElement(By.name("retype_password")).sendKeys("Qaz12345543");
        driver.findElement(By.cssSelector("input[value=\"Zarejestruj się\"]")).click();

        assertTrue(driver.getPageSource().contains("Hasła są różne"));
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
    }
}
